###关于FineUI
------------
基于 ExtJS 的专业 ASP.NET 控件库

###FineUI的使命
------------
创建 No JavaScript，No CSS，No UpdatePanel，No ViewState，No WebServices 的网站应用程序

###支持的浏览器
------------
Chrome、Firefox、Safari、IE 8.0+

###授权协议
------------
Apache License 2.0

注：ExtJS 库是在 GPL v3 协议下发布(http://www.sencha.com/license)。


###相关链接
------------
* 首页：http://fineui.com/
* 论坛：http://fineui.com/bbs/
* 示例：http://fineui.com/demo/
* 文档：http://fineui.com/doc/
* 下载：http://fineui.codeplex.com/

 

 
FineUI严格遵守 ExtJS 关于开源软件的规则，不再内置 ExtJS 库。

* 获取适用于 FineUI 的 ExtJS 库：http://fineui.com/bbs/forum.php?mod=viewthread&tid=3218 
* 基于 FineUI 的空项目（Net2.0 和 Net4.0 两个版本）：http://fineui.com/bbs/forum.php?mod=viewthread&tid=2123

 
为什么下载的源代码不能直接运行？
http://fineui.com/bbs/forum.php?mod=viewthread&tid=3218
 

 

###捐赠
------------
FineUI（开源版）作为一款开源软件已经持续开发了 8 年 并发布了 120 多个版本，并还将继续下去。如果你在商业软件中使用了FineUI（开源版），请捐赠作者以帮助 FineUI 的持续开发。

支付宝钱包扫码：

![FineUI](http://fineui.com/images/alipay.png)



###截图
------------
![FineUI](http://fineui.com/images/fineui.png)
